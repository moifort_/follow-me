export default function ({store, redirect}) {
  if (!store.state.login.isLogged) {
    return redirect('/')
  }
}
