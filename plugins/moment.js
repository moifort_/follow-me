import Vue from 'vue'
import VueMoment from 'vue-moment'
import moment from 'moment'
import 'moment/locale/fr'

Vue.use(VueMoment, {moment})
