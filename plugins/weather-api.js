import Vue from 'vue'
import darkSkyApi from 'dark-sky-api'

const darkSkyApiPlugin = {
  install(Vue) {
    if (Vue.__nuxt_darkskyapi_installed__) {
      return
    }
    Vue.__nuxt_darkskyapi_installed__ = true
    if (!Vue.prototype.$weather) {
      darkSkyApi.apiKey = process.env.darkSkyApi.apiKey
      darkSkyApi.units = 'ca'
      darkSkyApi.language = 'fr'
      Vue.prototype.$weather = darkSkyApi
    }
  }
}

Vue.use(darkSkyApiPlugin)

export default (ctx) => {
  const {app, store} = ctx
  app.$weather = Vue.prototype.$weather
  ctx.$weather = Vue.prototype.$weather
  store.$weather= Vue.prototype.$weather
}


