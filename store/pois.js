import uniqBy from 'lodash.uniqby'
import remove from 'lodash.remove'

let reference = null

export const state = () => ({
  pois: [],
})

export const actions = {
  init: function ({state, commit}, journeyId) {
    return new Promise((resolve, reject) => {
      commit('init')
      if (!journeyId) return reject('Missing journeyId')
      if (reference) {
        reference.off()
      }
      reference = this.$firebase.database().ref(`pois/${journeyId}`)
      reference.once('value').finally(() => resolve())
      reference.on('child_added', data => commit('add', {id: data.key, ...data.val()}))
      reference.on('child_changed', data => commit('update', {id: data.key, ...data.val()}))
      reference.on('child_removed', data => commit('remove', data.key))
    })

  },
  delete({}, id) {
    return new Promise((resolve, reject) => {
      if (!reference) reject("You should init first")
      reference.child(id).remove()
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
  update({}, poi) {
    return new Promise((resolve, reject) => {
      if (!reference) reject("You should init first")
      const id = poi.id
      const poiToUpdate = {...poi}
      delete poiToUpdate.id
      reference.child(id).update(poiToUpdate)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
  add({}, poi) {
    return new Promise((resolve, reject) => {
      if (!reference) reject("You should init first")
      reference.push(poi)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
}

export const mutations = {
  init: (state) => state.pois = [],
  add: (state, newPoi) => state.pois = uniqBy([...state.pois, newPoi], 'id'),
  update: (state, updatedPoi) => {
    const poiList = [...state.pois]
    const index = poiList.findIndex(poi => poi.id === updatedPoi.id)
    poiList[index] = updatedPoi
    state.pois = poiList
  },
  remove: (state, id) => {
    const removeIt = [...state.pois]
    remove(removeIt, (poi) => poi.id === id)
    state.pois = removeIt
  },
}
