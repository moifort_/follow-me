let reference = null

export const state = () => ({
  journey: {},
})


export const actions = {
  init: function ({state, commit}, journeyId) {
    return new Promise((resolve, reject) => {
      if (!journeyId) return reject('Missing journeyId')
      if (reference) {
        reference.off()
      }
      reference = this.$firebase.database().ref(`journeys/${journeyId}`)
      reference.on('value', function (snapshot) {
        let val = snapshot.val()
        if (!val) return reject()
        commit('set', {id: snapshot.key, ...val})
        resolve()
      })
    })
  },
}

export const mutations = {
  set: (state, newJourney) => state.journey = newJourney,
}
