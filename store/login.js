export const state = () => ({
  isLogged: false,
})

export const actions = {
  logout({commit}) {
    return new Promise(resolve => {
      this.$firebase.auth().signOut()
        .finally(() => commit('isLogged', false))
    })
  },
  login({commit, dispatch}, {password, email}) {
    return new Promise((resolve, reject) => {
      this.$firebase.auth().onAuthStateChanged((user) => commit('isLogged', !!user))
      this.$firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
          commit('isLogged', true)
          dispatch('user/init', userCredential.user.uid, {root: true})
            .finally(() => resolve())
        })
        .catch((error) => {
          commit('isLogged', false)
          reject(error.message)
        })
    })

  }
}

export const mutations = {
  isLogged: (state, isLogged) => state.isLogged = isLogged,
}
