import uniqBy from 'lodash.uniqby'
import remove from 'lodash.remove'

let reference = null

export const state = () => ({
  journeys: [],
})

export const getters = {
  get: (state) => (id) => {
    const index = state.journeys.findIndex(journey => journey.id === id)
    return state.journeys[index]
  },
}

export const actions = {
  init: function ({state, commit}, userId) {
    return new Promise((resolve, reject) => {
      if (!userId) return reject('Missing userId')
      if (reference) {
        reference.off()
      }
      reference = this.$firebase.database().ref(`journeys`)
      reference.orderByChild('member').equalTo(userId).once('value').finally(() => resolve())
      reference.orderByChild('member').equalTo(userId).on('child_added', data => commit('add', {id: data.key, ...data.val()}))
      reference.orderByChild('member').equalTo(userId).on('child_changed', data => commit('update', {id: data.key, ...data.val()}))
      reference.orderByChild('member').equalTo(userId).on('child_removed', data => commit('remove', data.key))
    })
  },
  delete({}, id) {
    return new Promise((resolve, reject) => {
      if (!reference) return reject('You should init first')
      reference.child(id).remove()
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
  update({}, journey) {
    return new Promise((resolve, reject) => {
      if (!reference) return reject('You should init first')
      const id = journey.id
      const journeyToUpdate = {...journey}
      delete journeyToUpdate.id
      reference.child(id).update(journeyToUpdate)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
  add({}, journey) {
    return new Promise((resolve, reject) => {
      if (!reference) return reject('You should init first')
      reference.push(journey)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
}

export const mutations = {
  add: (state, newJourney) => state.journeys = uniqBy([...state.journeys, newJourney], 'id'),
  update: (state, updatedJourney) => {
    const journeyList = [...state.journeys]
    const index = journeyList.findIndex(journey => journey.id === updatedJourney.id)
    journeyList[index] = updatedJourney
    state.journeys = journeyList
  },
  remove: (state, id) => {
    const removeIt = [...state.journeys]
    remove(removeIt, (journey) => journey.id === id)
    state.journeys = removeIt
  },
}
