export const state = () => ({
  id: '',
  name: '',
})


export const actions = {
  init({commit, state}, id) {
    return new Promise((resolve, reject) => {
      const userId = state.id || id || null
      if (!userId) return reject()
      this.$firebase.database().ref(`users/${userId}`).on('value', function (snapshot) {
        let val = snapshot.val()
        if (!val) return reject()
        commit('setId', userId)
        commit('setName', val.name)
        return resolve()
      })
    })
  },
  register({dispatch, commit}, {name, email, password}) {
    return new Promise((resolve, reject) => {
      this.$firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(userCredential => {
          const userId = userCredential.user.uid
          commit('setId', userId)
          return this.$firebase.database().ref(`users/${userId}`).update({name})
        })
        .then(() => {
          commit('setName', name)
          commit('setEmail', email)
          dispatch('init')
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  resetPassword({commit}, email) {
    return new Promise((resolve, reject) => {
      this.$firebase.auth().sendPasswordResetEmail(email)
        .then(() => resolve())
        .catch(error => reject(error))
    })
  },
}

export const mutations = {
  setId: (state, id) => state.id = id,
  setName: (state, name) => state.name = name,
}
