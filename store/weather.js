import moment from 'moment'

export const state = () => ({
  icon: null,
  temperature: null,
  windSpeed: null,
  status: 'INIT',
  updatedTime: null,
  longitude: null,
  latitude: null,
})

export const actions = {
  load({commit, state}, {longitude, latitude}) {
    const elapsedTime = moment().diff(state.updatedTime, 'minutes')
    if (longitude === state.longitude && latitude === state.latitude && elapsedTime < 30) return
    commit('setStatus', 'LOADING')
    this.$weather.loadCurrent({latitude, longitude})
      .then(weather => {
        commit('setIcon', weather.icon)
        commit('setTemperature', weather.temperature)
        commit('setWindSpeed', weather.windSpeed)
        commit('setUpdatedTime', Date.now())
        commit('setLatitude', latitude)
        commit('setLongitude',longitude)
        commit('setStatus', 'SUCCESS')
      })
      .catch(() => commit('setStatus', 'ERROR'))
  },
}

export const mutations = {
  setIcon: (state, icon) => state.icon = icon,
  setTemperature: (state, temperature) => state.temperature = temperature,
  setWindSpeed: (state, windSpeed) => state.windSpeed = windSpeed,
  setStatus: (state, status) => state.status = status,
  setUpdatedTime: (state, updatedTime) => state.updatedTime = updatedTime,
  setLatitude: (state, latitude) => state.latitude = latitude,
  setLongitude: (state, longitude) => state.longitude = longitude,
}
