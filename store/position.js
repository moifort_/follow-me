import moment from 'moment'


let reference = null

export const state = () => ({
  latitude: '',
  longitude: '',
  accuracy: '',
  updatedTime: null,
})

export const actions = {
  init({commit}, journeyId) {
    return new Promise((resolve, reject) => {
      if (!journeyId) return reject('Missing journeyId')
      if (reference) {
        reference.off()
      }
      reference = this.$firebase.database().ref(`positions/${journeyId}`)
      reference.on('value', function (snapshot) {
        let val = snapshot.val()
        if (!val) return resolve('No position')
        commit('setLatitude', val.latitude)
        commit('setLongitude', val.longitude)
        commit('setAccuracy', val.accuracy)
        commit('setUpdatedTime', val.updatedTime)
        resolve()
      })
    })
  },
  localise({commit, state}, force) {
    return new Promise((resolve, reject) => {
      if (!reference) return reject('You should init first')
      const elapsedTime = moment().diff(state.updatedTime, 'minutes')
      if (elapsedTime > 2 || force) {
        navigator.geolocation.getCurrentPosition(position => {
          const latitude = position.coords.latitude
          const longitude = position.coords.longitude
          const accuracy = Math.floor(position.coords.accuracy)
          const updatedTime = Date.now()
          reference.set({
            latitude,
            longitude,
            accuracy,
            updatedTime,
          }).then(() => {
            commit('setLatitude', latitude)
            commit('setLongitude', longitude)
            commit('setAccuracy', accuracy)
            commit('setUpdatedTime', updatedTime)
            resolve()
          }).catch((error) => reject(error.message))
        }, (error) => reject(error))
      } else {
        return resolve()
      }
    })
  },
}
export const getters = {
  get: state => {
    if (state.latitude == null || state.longitude == null) return null
    return [state.latitude, state.longitude]
  },
}

export const mutations = {
  setLatitude: (state, latitude) => state.latitude = latitude,
  setLongitude: (state, longitude) => state.longitude = longitude,
  setAccuracy: (state, accuracy) => state.accuracy = accuracy,
  setUpdatedTime: (state, updatedTime) => state.updatedTime = updatedTime,
}
