const functions = require('firebase-functions')
const {Storage} = require('@google-cloud/storage')
const storage = new Storage()
const sharp = require('sharp')
const path = require('path')
const os = require('os')

exports.generateThumbnail = functions.storage.object().onChange(event => {
  return new Promise((resolve, reject) => {
    const object = event.data
    const fileBucket = object.bucket
    const filePath = object.name
    const contentType = object.contentType
    const resourceState = object.resourceState
    const sizes = [86, 800]

    if (!contentType.startsWith('image/') || resourceState == 'not_exists') return reject('This is not an image')
    if (filePath.endsWith('_thumb.jpeg')) return reject('This is already a thumb')

    const fileName = filePath.split('/').pop()
    const bucket = storage.bucket(fileBucket)
    const tempFilePath = path.join(os.tmpdir(), fileName)

    return bucket.file(filePath).download({destination: tempFilePath})
      .then(() => {
        sizes.forEach(size => {
          const id = fileName.replace('.jpeg','')
          const newFileName = `${id}_${size}_thumb.jpeg`
          const newFileTemp = path.join(os.tmpdir(), newFileName)
          const newFilePath = `thumbs/${newFileName}`
          sharp(tempFilePath)
            .resize(size, null)
            .toFile(newFileTemp)
            .then(() => {
              bucket.upload(newFileTemp, {destination: newFilePath})
                .then(() => resolve())
                .catch(error => reject(error))
            })
            .catch(error => reject(error))
        })
      }).catch(error => reject(error))
  })
})
