# follow-me



## Build Setup

You should setup your environment variable:
* FIREBASE_API_KEY
* FIREBASE_AUTH_DOMAIN
* FIREBASE_DATABASE_URL
* FIREBASE_STORAGE_BUCKET
* ONESIGNAL_API_KEY

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Image CORS with firebase storage

* install gsutil: `curl https://sdk.cloud.google.com | bash`
* init: `gcloud init`
* send config `gsutil cors set cors.json gs://<your-cloud-storage-bucket>`
