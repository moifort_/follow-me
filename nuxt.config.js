const pkg = require('./package')


module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: pkg.description}
    ],
    link: [
      {rel: 'icon', type: 'image/png', href: '/favicon.png'},
    ],
    script: [
      {
        src: 'https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver,IntersectionObserverEntry',
        body: true
      },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},

  /*
  ** Global CSS
  */
  css: [
    'ant-design-vue/dist/antd.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/visibility',
    '@/plugins/antd-ui',
    '@/plugins/moment',
    '@/plugins/firebase',
    '@/plugins/vuex-persist',
    '@/plugins/weather-api',
    '@/plugins/leaflet',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/onesignal',
    '@nuxtjs/pwa',
    'nuxt-fontawesome',
    ['@nuxtjs/google-analytics', {
      id: 'UA-42286365-6',
      autoTracking: {exception: true},
      debug: {
        sendHitTask: process.env.NODE_ENV === 'PROD'
      },
    }]
  ],

  oneSignal: {
    cdn: true,
    OneSignalSDK: 'https://cdn.onesignal.com/sdks/OneSignalSDK.js',
    init: {
      appId: process.env.ONESIGNAL_API_KEY || '848c3998-87fa-422e-a293-d94e244e6cd3',
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
        disable: false
      }
    }
  },

  manifest: {
    lang: 'fr',
    start_url: '/user',
    description: "Suivez-moi pendant mes voyages!",
  },

  workbox: {
    offlineAssets: ['/_nuxt/img/*'],
  },

  fontawesome: {
    component: 'fa',
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      }
    ],
  },

  env: {
    firebase: {
      apiKey: process.env.FIREBASE_API_KEY,
      authDomain: process.env.FIREBASE_AUTH_DOMAIN,
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    },
    darkSkyApi: {
      apiKey: process.env.DARKSKYAPI_KEY,
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
